class UserMailer < ActionMailer::Base
  default :from => "naoresponder@icb.usp.br"

  def senha_reset(user)
    @usuario = user
    mail(:to=>user.email, :subject => "Recuperacao de senha")
  end

  def avisa_orientador (o, nome_usuario, u_id)
    @o = o
    @nome = nome_usuario
    @u_id = u_id
    mail(:to=>@o.email , :subject => "Autorizacao de usuario")
  end
end
