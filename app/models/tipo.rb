class Tipo < ActiveRecord::Base
  has_and_belongs_to_many :usuarios
  has_many :events
  validates_presence_of :nome
end
