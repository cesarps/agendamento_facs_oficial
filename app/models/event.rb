class Event < ActiveRecord::Base

  belongs_to :tipo
  belongs_to :usuario
  has_many :agendamentos, :dependent => :destroy
  validates_presence_of :tipo_id

  def odiainteiro
    if self.all_day?
      "Sim"
    else
      "Nao"
    end
  end


  scope :before,
        lambda {|end_time| {:conditions => ["starts_at < ?", Event.format_date(end_time)] }}
  scope :after,
        lambda {|start_time| {:conditions => ["ends_at > ?", Event.format_date(start_time)] }}


  # need to override the json view to return what full_calendar is expecting.
  # http://arshaw.com/fullcalendar/docs/event_data/Event_Object/
  def as_json(options = {})
    {
        :id => self.id,
        :title => self.title,
        #:description => self.description || "",
        :start => starts_at,
        #:end => ends_at,
        :recurring => false,
        :allDay => true,
        #:url => "http://watson.icb.usp.br/agendamento" << Rails.application.routes.url_helpers.event_path(id)
        :url => "http://localhost:3000" << Rails.application.routes.url_helpers.event_path(id)
        #:url => Rails.application.routes.url_helpers.event_path(id)        #redireciona para a página do evento quando clica no dia no calendario
    }
  end

  def self.format_date(date_time)
    Time.at(date_time.to_i).to_formatted_s(:db)
  end
end

