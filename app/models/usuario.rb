class Usuario < ActiveRecord::Base
  require "digest/sha2"
  attr_accessible :nome,:email,:senha,:senha_confirmation,:laboratorio, :orientador_id, :ramal, :foiaceito, :auth_token, :senha_reset_token,:senha_reset_sent_at, :type_id
  attr_accessor :senha_confirmation



  has_many :events, :dependent => :destroy
  has_and_belongs_to_many :tipos
  belongs_to :orientador

  validates_presence_of :nome
  validates_presence_of :email
  validates_presence_of :senha, :message => "Esta faltando senha"
  validates_uniqueness_of :email
  validates_format_of :email , :on => :create, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :message=> "Forma de email invalida"
  validates_confirmation_of :senha, :message => "Confirme a senha"

  before_create {|reg| reg.generate_token(:auth_token)}

  def send_senha_reset
    generate_token(:senha_reset_token)
    self.senha_reset_sent_at=Time.zone.now
    save!
    UserMailer.senha_reset(self).deliver
  end

  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while Usuario.exists?(column => self[column])
  end

  def criptografa_senha(senhanova)
    self.senha=Digest::SHA2.hexdigest("ICB2OlQu4Tr0#{senhanova}")
    save!
  end

end