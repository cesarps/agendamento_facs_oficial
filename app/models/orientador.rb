class Orientador < ActiveRecord::Base
  attr_accessible :nome, :email,:departamento
  has_many :usuarios
  validates_uniqueness_of :email,:message => "orientador ja cadastrado"
  validates_presence_of :nome
  validates_presence_of :email
  validates_presence_of :departamento

end
