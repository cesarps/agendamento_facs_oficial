module ApplicationHelper

  def session_usuario
    @session_usuario ||= Usuario.find(:first, :conditions => ['id = ?',session[:usuario]])
  end
end