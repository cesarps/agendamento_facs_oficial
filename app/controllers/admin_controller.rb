class AdminController < ApplicationController
  before_filter :usuario_autenticado?

  def show                 # permite o admin ver todos os eventos marcados
    @event = Event.find(:all)
    @agendamento = Agendamento.find(:all, :conditions => params[:event_id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @event }
      format.js { render :json => @event.to_json }
    end
  end

  def ver_usuarios          # pagina com todos os usuarios do sistema (vista pelo admin)
    @usuario = Usuario.find(:all)

  end

  def permissoes            # configura permissoes de usuario

    @user = Usuario.find(params[:id])
    @tipo = Tipo.find(:all)

  end
end
