class SenhaResetsController < ApplicationController

  def create
    usuario=Usuario.find_by_email(params[:email])
    usuario.send_senha_reset if usuario
    redirect_to root_url
    flash[:notice] = "Verifique seu e-mail"
  end

  def edit
    @usuario=Usuario.find_by_senha_reset_token!(params[:id])
    if @usuario.senha_reset_sent_at < 2.hours.ago
      redirect_to new_senha_reset_path, :notice => "Tempo de alteracao de senha expirado"
    end
  end

  def update
    @usuario=Usuario.find_by_senha_reset_token!(params[:id])
    if @usuario.senha_reset_sent_at < 2.hours.ago
      redirect_to new_senha_reset_path, :alert => "Tempo de alteracao de senha expirado"
    else
      usuario=Usuario.find_by_senha_reset_token!(params[:id])
      usuario.criptografa_senha(params[:usuario][:senha])
      redirect_to root_url, :notice => "Senha foi alterada"
    end
  end
end