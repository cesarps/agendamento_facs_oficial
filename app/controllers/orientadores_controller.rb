class OrientadoresController < ApplicationController


  # GET /orientadores
  # GET /orientadores.xml


  autocomplete :orientador, :nome

  def index
    @orientadores = Orientador.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @orientadores }
    end
  end

  # GET /orientadores/1
  # GET /orientadores/1.xml
  def show
    @orientador = Orientador.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @orientador }
    end
  end

  # GET /orientadores/new
  # GET /orientadores/new.xml
  def new
    @orientador = Orientador.new


    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @orientador }
    end
  end

  # GET /orientadores/1/edit
  def edit
    @orientador = Orientador.find(params[:id])
  end

  # POST /orientadores
  # POST /orientadores.xml
  def create
    @orientador = Orientador.new(params[:orientador])

    respond_to do |format|
      if @orientador.save
        format.html { redirect_to(@orientador, :notice => 'Orientador foi criado com sucesso.') }
        format.xml  { render :xml => @orientador, :status => :created, :location => @orientador }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @orientador.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /orientadores/1
  # PUT /orientadores/1.xml
  def update
    @orientador = Orientador.find(params[:id])

    respond_to do |format|
      if @orientador.update_attributes(params[:orientador])
        format.html { redirect_to(@orientador, :notice => 'Orientador foi atualizado com sucesso.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @orientador.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /orientadores/1
  # DELETE /orientadores/1.xml
  def destroy
    @orientador = Orientador.find(params[:id])
    @orientador.destroy

    respond_to do |format|
      format.html { redirect_to(orientadores_url) }
      format.xml  { head :ok }
    end
  end



end
