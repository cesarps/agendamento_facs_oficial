class LoginController < ApplicationController

  def login
    if request.post?
      @usuario = Usuario.find_by_email_and_senha(params[:email], Digest::SHA2.hexdigest("ICB2OlQu4Tr0#{params[:senha]}"))
      if @usuario
        session[:usuario] = @usuario.id
        session[:type_id] = @usuario.type_id
        session[:email] = @usuario.email
        session[:nome] = @usuario.nome

        if session[:return_to] && !session[:return_to].include(url_for(:action => "login"))
          redirect_to session[:return_to]
          session[:return_to] = nil
        else
          redirect_to :controller => "calendar", :action => "index"
        end

      else
        cookies.delete(:auth_token)
        session[:usuario]=nil
        session[:type_id]=nil
        session[:email] = nil
        session[:nome] = nil
        params[:login]=nil
        params[:senha]=nil
        flash[:notice]="Login ou senha invalidos"
      end
    end
  end

  def logout
    session[:usuario]=nil
    session[:type_id]=nil
    session[:email]= nil
    session[:nome] = nil
    redirect_to root_url
  end

end
