class ApplicationController < ActionController::Base
  protect_from_forgery
  # got these tips from
  # http://lyconic.com/blog/2010/08/03/dry-up-your-ajax-code-with-the-jquery-rest-plugin
  before_filter :correct_safari_and_ie_accept_headers

  after_filter :set_xhr_flash


  def usuario_autenticado?
    if session[:usuario].nil?
      redirect_to :controller => "calendar", :action =>"index"
      flash[:notice]="Cadastre-se e efetue o login"
    end
  end





  def set_xhr_flash
    flash.discard if request.xhr?
  end

  def correct_safari_and_ie_accept_headers
    ajax_request_types = ['text/javascript', 'application/json', 'text/xml']
    request.accepts.sort! { |x, y| ajax_request_types.include?(y.to_s) ? 1 : -1 } if request.xhr?
  end

  # geração de cookies para o esquecimento de senha

  def current_user
    @current_user ||= Usuario.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
  end



  #Validação do cadastro de eventos para evitar conflitos de horários e salas
  #A funçao verifica e sinaliza o que deve ser eliminado outra funçao que mata os registros
  def valida

    if @event.save
      @a = 0

      #busca ultimo agendamento,todos os eventos, todos os agendamentos exceto o ultimo
      #agen_novo = Agendamento.find(:last, :order => "updated_at")     # não esta trazendo a query certa...
      agen_novo = Agendamento.where("event_id in (?)", Event.last.id)         #compara com todos os agendamentos criados se tiver algum conflitando deve ser apagado
      @evento = Event.find(:last, :order => "updated_at")
      agendamentos = Agendamento.where("event_id not in (?)", Event.last.id)

      agen_novo.each do |agennew|
        agendamentos.each do |agen|

          if agen.event_id != @evento.id
            print "@a = " + @a.to_s + "\n"

            #compara o tipo de agendamento(sala)
            if agennew.tipo_id == agen.tipo_id
              #compara os horários e verifica se há sobreposição

              if agennew.data_inicio == agen.data_inicio

                if agennew.hora_inicio >= agen.hora_inicio && agennew.hora_inicio <= agen.hora_fim
                  @a = @a + 1

                end

                if agennew.hora_fim <= agen.hora_fim && agennew.hora_fim >= agen.hora_inicio
                  @a = @a + 1

                end

                if agennew.hora_inicio <= agen.hora_inicio && agennew.hora_fim >= agen.hora_fim
                  @a = @a + 1

                end
              end
            end

          end

          # Destroi o evento se houver conflito
          if @a > 0
            @evento.destroy
          end
        end
      end
    end
  end

end