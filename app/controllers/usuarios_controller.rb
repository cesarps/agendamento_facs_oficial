class UsuariosController < ApplicationController
  # GET /usuarios
  # GET /usuarios.xml


  skip_before_filter :usuario_autenticado?, :only => [:new, :create]
  # autocomplete :orientador, :nome



  def index
    @usuarios = Usuario.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @usuarios }
    end
  end

  # GET /usuarios/1
  # GET /usuarios/1.xml
  def show
    @usuario = Usuario.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @usuario }
    end
  end

  # GET /usuarios/new
  # GET /usuarios/new.xml
  def new
    @usuario = Usuario.new


    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @usuario }
    end
  end

  # GET /usuarios/1/edit
  def edit
    @usuario = Usuario.find(params[:id])
    orientador_id=@usuario.orientador_id
    @o=Orientador.find(orientador_id)


  end

  # POST /usuarios
  # POST /usuarios.xml
  def create
    @usuario = Usuario.new(params[:usuario])
    @usuario.senha=Digest::SHA2.hexdigest("ICB2OlQu4Tr0#{params[:usuario][:senha]}")
    @usuario.senha_confirmation = Digest::SHA2.hexdigest("ICB2OlQu4Tr0#{params[:usuario][:senha_confirmation]}")
    orientador_id=params[:usuario][:orientador_id]
    @o=Orientador.find(orientador_id)
    @nome=params[:usuario][:nome]


    respond_to do |format|
      if @usuario.save
        @u_id = @usuario.id
        UserMailer.avisa_orientador(@o, @nome,@u_id).deliver
        format.html { redirect_to(@usuario, :notice => 'Usuario criado com sucesso.') }
        format.xml  { render :xml => @usuario, :status => :created, :location => @usuario }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @usuario.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /usuarios/1
  # PUT /usuarios/1.xml
  def update
    @usuario = Usuario.find(params[:id])
    @usuario.senha=Digest::SHA2.hexdigest("ICB2OlQu4Tr0#{params[:usuario][:senha]}")
    @usuario.senha_confirmation = Digest::SHA2.hexdigest("ICB2OlQu4Tr0#{params[:usuario][:senha_confirmation]}")
    orientador_id=params[:usuario][:orientador_id]
    @o=Orientador.find(orientador_id)
    @nome=params[:usuario][:nome]

    respond_to do |format|
      if @usuario.update_attributes(params[:usuario])
        @u_id=@usuario.id
        UserMailer.avisa_orientador(@o,@nome,@u_id).deliver
        format.html { redirect_to(@usuario, :notice => 'Usuario atualizado com sucesso.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @usuario.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.xml
  def destroy
    @usuario = Usuario.find(params[:id])
    @usuario.destroy

    respond_to do |format|
      format.html { redirect_to(usuarios_url) }
      format.xml  { head :ok }
    end
  end

  def permite_tudo
    @u=Usuario.find(params[:id])
    @t=Tipo.find(:all)
    @tu=TipoUsuario.find_all_by_usuario_id(params[:id])
    if @tu.empty?
      @t.each do |t|
        @tu=TipoUsuario.new
        @tu.tipo_id = t.id
        @tu.usuario_id = @u.id
        @tu.save
      end
    else
      redirect_to :controller => 'tipo_usuarios', :action => 'new', :id => @u.id
      return
    end
    redirect_to :controller => 'tipo_usuarios', :action => 'new', :id => @u.id
  end

  def negar_tudo
    @u=Usuario.find(params[:id])
    @tu=TipoUsuario.find_all_by_usuario_id(params[:id])
    @tu.each do |tu|
      tu.destroy
    end
    redirect_to :controller => 'tipo_usuarios', :action => 'new', :id => @u.id
  end
end
