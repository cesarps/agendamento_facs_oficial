class CreateUsuarios < ActiveRecord::Migration
  def self.up
    create_table :usuarios do |t|
      t.string :nome
      t.string :email
      t.string :senha
      t.string :laboratorio
      t.string :orientador
      t.string :ramal
      t.string :departamento
      t.string :auth_token
      t.string :senha_reset_token
      t.datetime :senha_reset_sent_at
      t.string :type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :usuarios
  end
end
