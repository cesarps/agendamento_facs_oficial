class AddPendenteUemailToEvents < ActiveRecord::Migration
  def self.up
    add_column :events, :u_email, :string
    add_column :events, :pendente, :boolean
  end

  def self.down
    remove_column :events, :pendente
    remove_column :events, :u_email
  end
end
