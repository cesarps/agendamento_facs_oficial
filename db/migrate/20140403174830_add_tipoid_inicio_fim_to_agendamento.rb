class AddTipoidInicioFimToAgendamento < ActiveRecord::Migration
  def self.up
    add_column :agendamentos, :tipo_id, :boolean
    add_column :agendamentos, :inicio, :string, :size => 10
    add_column :agendamentos, :fim, :string, :size => 10
  end

  def self.down
    remove_column :agendamentos, :fim
    remove_column :agendamentos, :inicio
    remove_column :agendamentos, :tipo_id
  end
end
