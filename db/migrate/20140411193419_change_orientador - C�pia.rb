class ChangeOrientador < ActiveRecord::Migration
  def self.up
    rename_column :usuarios, :orientador, :orientador_id
    rename_column :orientadores, :departamento, :departamento_id
  end

  def self.down
    rename_column :usuarios, :orientador_id, :orientador
    rename_column :orientadores, :departamento_id, :departamento
  end
end
