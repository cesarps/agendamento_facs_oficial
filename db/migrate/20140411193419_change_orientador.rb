class ChangeOrientador < ActiveRecord::Migration
  def self.up
    rename_column :usuarios, :orientador, :orientador_id
  end

  def self.down
    rename_column :usuarios, :orientador_id, :orientador
  end
end
