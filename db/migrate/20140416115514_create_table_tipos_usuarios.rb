class CreateTableTiposUsuarios < ActiveRecord::Migration
  def self.up
    create_table :tipos_usuarios, id:false do |t|
      t.belongs_to :tipo
      t.belongs_to :usuario
    end
  end

  def self.down
    drop_table :tipos_usuarios
  end
end


