class AddUsuarioIdToEvents < ActiveRecord::Migration
  def self.up
    add_column :events, :usuario_id, :integer
  end

  def self.down
    remove_column :events, :usuario_id
  end
end
