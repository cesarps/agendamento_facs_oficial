class RemoveCamposDeEvents < ActiveRecord::Migration
  def self.up
    remove_column :events, :title
    remove_column :events, :all_day
    remove_column :events, :domingo
    remove_column :events, :segunda
    remove_column :events, :terca
    remove_column :events, :quarta
    remove_column :events, :quinta
    remove_column :events, :sexta
    remove_column :events, :sabado
  end

  def self.down
    add_column :events, :title, :string
    add_column :events, :all_day, :integer
    add_column :events, :domingo, :integer
    add_column :events, :segunda, :integer
    add_column :events, :terca, :integer
    add_column :events, :quarta, :integer
    add_column :events, :quinta, :integer
    add_column :events, :sexta, :integer
    add_column :events, :sabado, :integer
  end
end
