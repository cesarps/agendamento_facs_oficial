class CreateOrientadores < ActiveRecord::Migration
  def self.up
    create_table :orientadores do |t|
      t.string :nome
      t.string :email
      t.integer :departamento

      t.timestamps
    end
  end

  def self.down
    drop_table :orientadores
  end
end
