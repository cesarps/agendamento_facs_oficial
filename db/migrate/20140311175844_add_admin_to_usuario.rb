class AddAdminToUsuario < ActiveRecord::Migration
  def self.up
    add_column :usuarios, :type_id,:integer

  end

  def self.down
    remove_column :usuarios, :type_id
  end
end