require 'test_helper'

class OrientadoresControllerTest < ActionController::TestCase
  setup do
    @orientador = orientadores(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:orientadores)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create orientador" do
    assert_difference('Orientador.count') do
      post :create, :orientador => @orientador.attributes
    end

    assert_redirected_to orientador_path(assigns(:orientador))
  end

  test "should show orientador" do
    get :show, :id => @orientador.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @orientador.to_param
    assert_response :success
  end

  test "should update orientador" do
    put :update, :id => @orientador.to_param, :orientador => @orientador.attributes
    assert_redirected_to orientador_path(assigns(:orientador))
  end

  test "should destroy orientador" do
    assert_difference('Orientador.count', -1) do
      delete :destroy, :id => @orientador.to_param
    end

    assert_redirected_to orientadores_path
  end
end
